@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Editing Company
                    </div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if ( $errors->count() > 0 )
                            <p>The following errors have occurred:</p>

                            <ul>
                                @foreach( $errors->all() as $message )
                                    <li>{{ $message }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <form class="form-horizontal" action="{{url('/companies/'.$company->id)}}" method="post" enctype="multipart/form-data">
                            <fieldset>
                            {{ method_field('PUT') }}
                            {{csrf_field()}}
                            <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Name</label>
                                    <div class="col-md-4">
                                        <input id="textinput" value="{{$company->name}}" name="name" type="text" placeholder="" class="form-control input-md" required="">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="email">Email</label>
                                    <div class="col-md-4">
                                        <input id="email" name="email" value="{{$company->email}}" type="text" placeholder="" class="form-control input-md" required="">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="website">Website</label>
                                    <div class="col-md-4">
                                        <input id="website" name="website" value="{{$company->website}}" type="text" placeholder="" class="form-control input-md" required="">
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="website">Logo</label>
                                    <div class="col-md-4">
                                        <input id="logo" name="logo" type="file" class="form-control input-md" required="">
                                    </div>

                                </div>
                                <!-- Text input-->
                                <div class="form-group" style="text-align: center">
                                    <img src="{{url('/public/'.$company->logo)}}" width="200" height="200">
                                </div>
                                <!-- Text input-->
                                <div class="form-group">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="submit" class="form-control btn btn-success" value="Submit">
                                    </div>
                                </div>

                            </fieldset>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
