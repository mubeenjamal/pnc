@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Company Listing
                    <span class="pull-right">
                        <a href="{{url('/companies/create')}}"> <button class="btn btn-success">Add</button></a>
                    </span>
                </div>

                <div class="panel-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif

                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Logo</th>
                                    <th scope="col">Website</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($companies))
                                    @foreach($companies as $company)
                                <tr>
                                    <th scope="row">{{$company->id}}</th>
                                    <td>{{$company->name}}</td>
                                    <td>{{$company->email}}</td>
                                    <td><img src="{{url('public/'.$company->logo)}}" width="100" height="100" class="img img-responsive"/> </td>
                                    <td>{{$company->website}}</td>
                                    <td>
                                        <a href="{{url('/companies/'.$company->id.'/edit')}}"><input type="button" class="btn btn-primary" value="Edit"></a>
                                        <form action="{{ route('companies.destroy', $company->id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>


                                    </td>
                                </tr>
                                @endforeach
                                    @else
                                    <tr>
                                        <td colspan="5">There is no record</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
