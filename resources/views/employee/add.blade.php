@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Adding Employee
                    </div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                            @if ( $errors->count() > 0 )
                                <p>The following errors have occurred:</p>

                                <ul>
                                    @foreach( $errors->all() as $message )
                                        <li>{{ $message }}</li>
                                    @endforeach
                                </ul>
                            @endif
                            <form class="form-horizontal" action="{{url('/employees')}}" method="post" enctype="multipart/form-data">
                                <fieldset>
                                    {{csrf_field()}}
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="textinput">First Name</label>
                                        <div class="col-md-4">
                                            <input id="textinput" name="first_name" type="text" placeholder="" class="form-control input-md" required="">

                                        </div>
                                    </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="textinput">Last Name</label>
                                            <div class="col-md-4">
                                                <input id="textinput" name="last_name" type="text" placeholder="" class="form-control input-md" required="">

                                            </div>
                                        </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="email">Email</label>
                                        <div class="col-md-4">
                                            <input id="email" name="email" type="text" placeholder="" class="form-control input-md" required="">

                                        </div>
                                    </div>
                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="email">Phone</label>
                                            <div class="col-md-4">
                                                <input id="email" name="phone" type="number" placeholder="" class="form-control input-md" required="">

                                            </div>
                                        </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="website">Company ID</label>
                                        <div class="col-md-4">
                                            <select class="form-control" name="company_id">
                                                @if(count($companies))
                                                    @foreach($companies as $company)
                                                    <option value="{{$company->id}}">{{$company->id}}</option>
                                                    @endforeach
                                                    @endif
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <div class="col-md-4">
                                        </div>
                                        <div class="col-md-4">
                                            <input type="submit" class="form-control btn btn-success" value="Submit">
                                        </div>
                                    </div>

                                </fieldset>
                            </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
