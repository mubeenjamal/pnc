@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Editing Employee
                    </div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if ( $errors->count() > 0 )
                            <p>The following errors have occurred:</p>

                            <ul>
                                @foreach( $errors->all() as $message )
                                    <li>{{ $message }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <form class="form-horizontal" action="{{url('/employees/'.$employee->id)}}" method="post" enctype="multipart/form-data">
                            <fieldset>
                            {{ method_field('PUT') }}
                            {{csrf_field()}}
                            <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">First Name</label>
                                    <div class="col-md-4">
                                        <input id="textinput" value="{{$employee->first_name}}" name="first_name" type="text" placeholder="" class="form-control input-md" required="">

                                    </div>
                                </div>
                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Last Name</label>
                                    <div class="col-md-4">
                                        <input id="textinput" value="{{$employee->last_name}}" name="last_name" type="text" placeholder="" class="form-control input-md" required="">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="email">Email</label>
                                    <div class="col-md-4">
                                        <input id="email" name="email" value="{{$employee->email}}" type="text" placeholder="" class="form-control input-md" required="">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="website">Phone</label>
                                    <div class="col-md-4">
                                        <input id="phone" name="phone" value="{{$employee->phone}}" type="text" placeholder="" class="form-control input-md" required="">
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="submit" class="form-control btn btn-success" value="Submit">
                                    </div>
                                </div>

                            </fieldset>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
