<?php

namespace App\Http\Controllers;
use App\Mail\NewUserNotificatio;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Employee;
use App\Company;
use Validator;
class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();
        $data = [
            'employees' => $employees
        ];
        return view('employee/content',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::all();
        $data = [
            'companies' => $companies
        ];
        return view('employee/add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'company_id' => 'required',
        ];
        $customMessages = [
            'required' => 'The :attribute field is mandatory.'
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return view('employees/add')->withErrors($errors);
        }
        $input = $request->all();
        $employee = Employee::create($input);
        if($employee->id){
            Mail::to($input['email'])->send(new NewUserNotificatio);
            return redirect()->route('employees.index');
        }else{
            dd('Failed to add');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);
        $data = [
            'employee' => $employee
        ];
        return view('employee/edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = Employee::find($id);
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $save = $employee->save();
        if($save)
            return redirect()->route('employees.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del = Employee::destroy($id);
        return redirect()->route('employees.index');
    }
}
