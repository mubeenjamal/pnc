<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
class GeneralController extends Controller
{
    //

    public function sendMail(Request $request){

        $data = array(
            'name' => $request->input('name'),
            'email' => $request->input('email')
        );
        Mail::send('mail', $data, function($message) {
            $message->to('codeio2019@gmail.com', 'Work with us')->subject
            ('There is testing email');
            $message->from('support@codeio.co','Contact Form');
        });
        return redirect()->to(env('APP_URL').'')->send();
    }
}
