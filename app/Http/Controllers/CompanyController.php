<?php

namespace App\Http\Controllers;
use App\Mail\NewUserNotificatio;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Company;
use Illuminate\Support\Facades\Redirect;
use Validator;
use File;
class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();
        $data = [
            'companies' => $companies
        ];

        return view('company/content',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'website' => 'required',
        ];
        $customMessages = [
            'required' => 'The :attribute field is mandatory.'
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return view('company/add')->withErrors($errors);
        }
        $input = $request->all();
        /*Upload Image*/
        if(request()->hasFile('logo')){
            $folder = '/uploads/company';
            $image = $request->file('logo');
            $name = md5(time()).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path("{$folder}");
            $image->move($destinationPath, $name);
            $input['logo'] = $folder.'/'.$name;
        }
        $company = Company::create($input);
        if($company->id){
            Mail::to($input['email'])->send(new NewUserNotificatio);
            return redirect()->route('companies.index');
        }else{
            dd('Failed to add');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        $data = [
            'company' => $company
        ];
        return view('company/edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::find($id);
        $company->name = $request->name;
        $company->email = $request->email;
        $company->website = $request->website;
        if(request()->hasFile('logo')){
            $image_path = public_path($company->logo);
            if(File::exists($image_path)) {
                File::delete($image_path);
            }
            $folder = '/uploads/company';
            $image = $request->file('logo');
            $name = md5(time()).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path("{$folder}");
            $image->move($destinationPath, $name);
            $company->logo = $folder.'/'.$name;
        }
        $save = $company->save();
        if($save)
            return redirect()->route('companies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        $image_path = public_path($company->logo);
        if(File::exists($image_path)) {
            File::delete($image_path);
        }
        $del = Company::destroy($id);
        return redirect()->route('companies.index');
    }
}
